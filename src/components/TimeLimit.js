import { useState, useEffect } from 'react';

const TimeLimit = (props) => {
  const [progress, setProgress] = useState(props.value + props.countdown);

  useEffect(() => {
    props.stopTimer === true && props.setAnsTime(progress);

    const timer = () => {
      setTimeout(() => {
        setProgress(progress - 1);
        // props.setTimeLeft(progress);
      }, 1000)
    };

    props.stopTimer === false && progress > 0 && timer();

    // Clear timeout if the component is unmounted
    return () => clearTimeout(timer);
  });

  return (
    progress <= props.value &&
    <p>You have <span className="timer-progress">{Math.round(progress)}</span> second{progress > 1 && 's'} left.</p>
  );
}

export default TimeLimit;
