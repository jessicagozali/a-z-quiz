import {
  createContext,
  useReducer
} from 'react';
import { quizReducer } from '../reducers/quizReducer';

export const AnsPoolContext = createContext();

export const AnsPoolProvider = ({ children }) => {
  const [store, dispatch] = useReducer(quizReducer, []);

  return (
    <AnsPoolContext.Provider value={{store, dispatch}}>
      {children}
    </AnsPoolContext.Provider>
  );
};
