# A-Z Quiz

&nbsp;    

>
> Live website link: [https://azquiz.jessicagozali.com.au/](https://azquiz.jessicagozali.com.au/)
>

&nbsp;    

## Purpose

This A-Z Quiz app is a simulation of a childhood game where participants would need to write down items in some categories that start with certain letter.

&nbsp;    

---

&nbsp;    

## Features

&nbsp;    

![A-Z Quiz Demo](./img/a-z-quiz-demo.gif)

&nbsp;    

Categories available:

- Country (Rest Countries API: https://restcountries.com/v3.1/name/)
- Capital City (Rest Countries API: https://restcountries.com/v3.1/capital/)
- Animal (local JSON file)
- Fruit Veggie (local JSON file)
- Movie Title (OMDb API: https://www.omdbapi.com/?t=)
- Colour Name (Color Names API: https://api.color.pizza/v1/names/)
- Sports (local JSON file)
- Musical Instrument (local JSON file)

Default game round: 3

A unique category will be picked for each round, and answer from each round will be collected before checking them against the answer pool or API.

&nbsp;    

![Score calculation](./img/score-calculation.gif)

&nbsp;    

Ten points will be given for each answer that exists in the pool or in the API responses.

## Tech Stack

Framework:

- React JS with Material UI

Deployment:

- Netlify

Other tools:

- VS Code
- Bitbucket

## Acknowledgements

API used in the app belong and all credits are given to the respective owner.

- [Rest Countries API](https://restcountries.com/)
- [OMDb API](https://www.omdbapi.com/)
- [Color Names API](https://github.com/meodai/color-names)

